<?php
/*************************************************************
Script to crawle links from sitemap from https://www.asus.com/ua/ website and populate a db
Written by Andrii Nebylovych
*************************************************************/
// crawling library with changed line 75: I had a problem with "file_get_contents(): stream does not support seeking", so changed line 75 in simple_php_dom, according to answer here https://stackoverflow.com/questions/42685814/file-get-contents-stream-does-not-support-seeking-when-was-php-behavior-abo

require_once('simple_html_dom.php');
// function for checking if the page is a product page

define('SITEMAP', ['https://www.asus.com/ua/sitemap/ua1.xml', 'https://www.asus.com/ua/sitemap/ua2.xml', 'https://www.asus.com/ua/sitemap/ua3.xml', 'https://www.asus.com/ua/sitemap/ua4.xml', 'https://www.asus.com/ua/sitemap/ua5.xml', 'https://www.asus.com/ua/sitemap/ua6.xml',]);

// define unique ID that defferentiates product page from none product one
define('SPECIAL_ID', 'product-topinfo');

/*************************************************************
    End of user defined settings.
*************************************************************/
// DB Setup and DB related functions
$dbHost = "localhost";
$dbUsername = "root";
$dbPassword = "";
$dbName = "website_parsing";

//// Create connection
$conn = mysqli_connect($dbHost, $dbUsername, $dbPassword, $dbName);

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
echo "Connected DB $dbName successfully\n";

function insertRow($url, $isProduct, $productName){
    global $conn;
    $sql = "INSERT INTO sitemap (url,isProduct, productName) VALUES ('".$url."','".$isProduct."','".$productName."')";
    if(mysqli_query($conn, $sql)){
        echo "Records inserted successfully.\n";
    } else{
        echo "ERROR: Could not able to execute $sql.  ". mysqli_error($conn);
    }
}

function returnRowUrl($url){
    global $conn;
    $sql = "SELECT url FROM sitemap WHERE url = '$url'";
    $result = mysqli_query($conn, $sql);
    return $result;
}

/*************************************************************
    End of db setup
*************************************************************/
// function to check if a page is a product page or not
// takes 2 parameters: Site to check and Uniques ID that distinquish product from non-product pages
function productStats($SITE, $SPECIAL_ID){

	// getting a site
    $html = file_get_html($SITE);

	// creating retunr object
	$returnValues = array();

	// getting isProduct
	// getting right element
    $checkIfProduct = $html->getElementById($SPECIAL_ID);
    if($checkIfProduct !== NULL){
        $returnValues["isProduct"] = TRUE;
    }
    else {
        $returnValues["isProduct"] = FALSE;
    }

	// getting productName
	// getting right element
	$productName = $html->find('#ctl00_ContentPlaceHolder1_ctl00_span_model_name', 0)->plaintext;
	$returnValues["productName"] = $productName;

	//for testing
	// print_r($returnValues);

	return $returnValues;

}

/*************************************************************
    End of helpers functions
*************************************************************/
// working with sitemap
function parsing(){
	foreach(SITEMAP as $siteMapPart){
		// test
		echo "working with this SITEMAP now: ".($siteMapPart)."\n";

		// Interprets an XML file into an object
		try{
			$xml = simplexml_load_file($siteMapPart);
		} catch (Exception $e){
			echo 'Caught exception: ',  $e->getMessage(), "\n";
			continue;
		}

		// loop over links in sitemap files
		// limit number of pages for testing
		//for ($i = 0; $i < 10; $i++){

		for ($i = 0; $i < count($xml); $i++){

			// variable for links
			$url = $xml->url[$i]->loc;

			// test
			echo "working with this PAGE now: ".($url)."\n";

			// check if th url is in the DB already
			// if not in the DB add to the DB
			if(returnRowUrl($url)->num_rows == 0){
					// get values of a product
					$productValues = productStats($url, SPECIAL_ID);

					// insert into DB values
					insertRow($url, $productValues["isProduct"], $productValues["productName"]);
				}
				else{
					echo "there is a link in the DB already ".$url."\n";
				}
		} // for each link
	}
}


/*************************************************************
    End of crawling logic
*************************************************************/
// INITIALIZATION
parsing();

mysqli_close($conn);
/*************************************************************
    End of INITIALIZATION
*************************************************************/















